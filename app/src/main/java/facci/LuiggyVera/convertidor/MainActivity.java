package facci.LuiggyVera.convertidor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;


public class MainActivity extends Activity {
    EditText EditTextvalorconvertir;
    CheckBox Checkcaf, Checkfac;
    Button Buttonvalidarcampo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Checkcaf = (CheckBox) findViewById(R.id.Checkcaf);
        Checkfac = (CheckBox) findViewById(R.id.Checkfac);
        EditTextvalorconvertir = (EditText) findViewById(R.id.EditTextvalorconvertir);
        Buttonvalidarcampo = (Button) findViewById(R.id.Buttonvalidarcampo);

        // Se realiza la funcion del boton de conversion
        Buttonvalidarcampo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Checkcaf.isChecked ()) {
                    int valor = Integer.parseInt(EditTextvalorconvertir.getText().toString());
                    float resultadoFahrenheit = (float) ((valor * 1.8) + 32);
                    Toast.makeText(getApplicationContext(),"El resultado es: " + resultadoFahrenheit + " °F", Toast.LENGTH_SHORT).show();
                }else if(Checkfac.isChecked ()){
                    int valor = Integer.parseInt(EditTextvalorconvertir.getText().toString());
                    float resultadoCelsius = (float) ((valor - 32)/(1.8));
                    Toast.makeText(getApplicationContext(),"El resultado es: " + resultadoCelsius +" °C", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Seleccione un casillero", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}

